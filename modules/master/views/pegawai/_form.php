<?php

use app\modules\master\models\Agama;
use app\modules\master\models\Divisi;
use app\modules\master\models\Jabatan;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\master\models\Pegawai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pegawai-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'id_agama')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Agama::find()->all(),'id','nama'),
        'options' => ['placeholder' => 'Pilih Agama ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php echo $form->field($model, 'id_divisi')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Divisi::find()->all(),'id','nama'),
        'options' => ['placeholder' => 'Pilih Divisi ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php echo $form->field($model, 'id_jabatan')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Jabatan::find()->all(),'id','nama'),
        'options' => ['placeholder' => 'Pilih Jabatan ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'id_province')->dropDownList($dataProvince,[
        'prompt'=>'-Choose a province-',
    ]); ?>

    <?= $form->field($model, 'id_city')->dropDownList([],[
        'prompt'=>'-Choose a city-',
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs('
$("#pegawai-id_city").attr("disabled",true);
$("#pegawai-id_province").change(function() {
    $.get("'.Url::to(['get-cities','province_id'=>'']).'" + $(this).val(), function(data)
    {
        select = $("#pegawai-id_city")
        select.empty();
        var options = "<option value=\'\'>-Choose a city-</option>";
        $.each(data.cities, function(key, value) {
            options += "<option value=\'"+value.id+"\'>"+ value.name +"</option>";
        });
        select.append(options);
        $("#pegawai-id_city").attr("disabled",false);
    });
});
');